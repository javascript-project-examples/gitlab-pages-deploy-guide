# Gitlab Pages Deploy guide for HTML pages

## 1. Create deploy script
Create a file in the root of your project and call it: `.gitlab-ci.yml`

> **Note:** Be sure to include the dot (.)

```
image: alpine:latest


pages:
  stage: deploy
  script:
  - mkdir -p .dist
  - cp -r * .dist
  - mv .dist public
  artifacts:
    paths:
    - public
  only:
  - master
```
Fig: .gitlab-ci.yml

## 2. Validate YML Syntax
You may check the syntax for your yml file on the Gitlab website.
[Gitlab CI Syntax Validation](https://gitlab.com/javascript-project-examples/javascript-example-vanillajs-todo-app/-/ci/lint)

## 3. Push to master
Once you've created your `.gitlab-ci.yml` file, be sure to add it to the repository. 

## 4. Find your site
Your site will be hosted on the following domain: `https://<username>.gitlab.io/<repositoryname>`

> **Side note:** If your username contains a dot (.) you will might not be able to use this feature.
